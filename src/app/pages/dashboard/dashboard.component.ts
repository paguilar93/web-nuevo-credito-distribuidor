import {Component, OnInit} from '@angular/core';
import {ICompanyData} from 'src/app/services/company/companyDataInterface';
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  doClean = false;
  searchError: string | null = null;
  companies: ICompanyData[] = [];
  filteredCompanyNames: string[] = ['Coca Cola Company', 'Motranza', 'Petroleos y Servicios'];
  companyNames: string[] = ['Coca Cola Company', 'Motranza', 'Petroleos y Servicios'];

  companyName: String | null = null;
  company = '';
  matched = false;

  constructor(private router: Router) {
    let savedValue = sessionStorage.getItem('company');
    if (savedValue) {
      this.companyName = savedValue;
    } else {
      this.router.navigateByUrl('').then(r => console.log('r', r));
    }
  }

  ngOnInit(): void {
  }

  goOutside(): void {
    this.router.navigateByUrl('').then(r => console.log('r', r));
  }
}
