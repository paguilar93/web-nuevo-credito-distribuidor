import {Component, OnInit} from '@angular/core';
import {ICompanyAdc} from 'src/app/services/adc/companyAdcInterface';
import {AdcService} from 'src/app/services/adc/adc.service';
import {Router} from "@angular/router";
import {CompanyService} from "../../services/company/company.service";

@Component({
  selector: 'app-quota-consultation',
  templateUrl: './quota-consultation.component.html'
})
export class QuotaConsultationComponent implements OnInit {
  doClean = false;
  searchError: string | null = null;
  companiesData: any[] = [];
  selectedCompany: ICompanyAdc | null = null;
  matched = false;
  companyName: String | null = null;
  companyRuc: String | null = null;

  constructor(private adcService: AdcService, private router: Router, private companyService: CompanyService) {
    let savedValue = sessionStorage.getItem('company');
    let savedRuc = sessionStorage.getItem('ruc');
    if (savedRuc) {
      this.companyRuc = savedRuc;
    }
    if (savedValue) {
      this.companyName = savedValue;
      this.getCompanies();
    } else {
      this.router.navigateByUrl('').then(r => console.log('r', r));
    }
  }

  ngOnInit(): void {
  }

  getCompanies(): void {
    this.companyService.getCompaniesAdc().subscribe((data) => {
      this.companiesData = data;
      this.selectedCompany = this.companiesData.filter((item) => item.name.includes(this.companyName))[0];
      console.log('selected', this.selectedCompany);

    });
  }

  searchId(id: number): void {
    this.adcService.getCompaniesAdcData().subscribe((data) => {
      this.companiesData = data;
      console.log('companies', this.companiesData);
      if (this.companiesData.length) {
        for (let i = 0; i < this.companiesData.length - 1; i++) {
          if (this.companiesData[i].id_number === id) {
            this.matched = true;
            this.selectedCompany = this.companiesData[i];
          }
        }
        if (!this.matched) {
          this.searchError = 'Empresa no encontrada. Intente nuevamente';
          this.selectedCompany = null;
        }
      }
    });
  }

  clean(clean: boolean): void {
    this.doClean = clean;
    this.searchError = null;
    this.selectedCompany = null;
  }
}
