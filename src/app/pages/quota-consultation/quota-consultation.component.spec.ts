import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotaConsultationComponent } from './quota-consultation.component';

describe('QuotaConsultationComponent', () => {
  let component: QuotaConsultationComponent;
  let fixture: ComponentFixture<QuotaConsultationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuotaConsultationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotaConsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
