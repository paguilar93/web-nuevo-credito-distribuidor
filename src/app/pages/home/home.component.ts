import {Component, OnInit} from '@angular/core';
import {ICompanyData} from 'src/app/services/company/companyDataInterface';
import {CompanyService} from 'src/app/services/company/company.service';
import {Router} from "@angular/router";
import {NotifierComponent} from 'src/app/components/notifier/notifier.component';
import {MatSnackBar} from '@angular/material/snack-bar';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  doClean = false;
  searchError: string | null = null;
  companies: ICompanyData[] = [];
  filteredCompanyNames: string[] = ['Coca Cola Company', 'Motranza', 'Petroleos y Servicios'];
  companyNames: string[] = ['Coca Cola Company', 'Motranza', 'Petroleos y Servicios'];

  selectedCompany: ICompanyData | null = null;
  company = '';
  user = null;
  password = null;
  matched = false;

  constructor(private companyService: CompanyService, private router: Router, private snackBar: MatSnackBar) {
    this.getCompanies();
  }

  ngOnInit(): void {

  }

  getCompanies(): void {
    this.companyService.getCompaniesData().subscribe((data) => {
      this.companies = data;
      this.filteredCompanyNames = this.companies.map((item) => item.name);
      this.companyNames = this.companies.map((item) => item.name);
    });
  }

  goInside():void{
    let company = this.companies.find((item) => item.user === this.user && item.password === this.password);
    if(company){
      console.log('company',company);
      sessionStorage.setItem('company', company.name);
      sessionStorage.setItem('ruc', company.id_number.toString());
      sessionStorage.setItem('type', company.type.toString());
      this.router.navigateByUrl(' ').then(r => console.log('r',r));
    }else{
      this.snackBar.openFromComponent(NotifierComponent, {
        data: {
          message: 'Usuario o contraseña incorrectos',
          dismiss: 'Cerrar',
          type: 'Aviso',
        },
        duration: 1300,
        panelClass: 'alert-danger'
      });
    }

  }

  changedCompanySearch(): void {
    if (this.company !== null && this.company !== '') {
      this.filteredCompanyNames = this.companyNames.filter((item) => item.includes(this.company));
    } else {
      this.filteredCompanyNames = this.companyNames;

    }
  }

  clean(clean: boolean): void {
    this.doClean = clean;
    this.searchError = null;
    this.selectedCompany = null;
  }
}
