import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICompanyData } from './companyDataInterface';
import {ICompanyAdc} from "../adc/companyAdcInterface";

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  private companyIdsUrl = 'assets/data/companyData.json';
  private companyAdcUrl = 'assets/data/companyAdcData.json';
  constructor(private http: HttpClient) { }
  getCompaniesData(): Observable<ICompanyData[]> {
    return this.http.get<ICompanyData[]>(this.companyIdsUrl);
  }

  getCompaniesAdc(): Observable<ICompanyAdc[]> {
    return this.http.get<ICompanyAdc[]>(this.companyAdcUrl);
  }
}
