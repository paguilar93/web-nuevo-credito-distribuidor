export interface IItem {
  id: number;
  type: string;
  regional: string;
  company: string;
  identification: string;
  client_provider: string;
  invoice_number: string;
  invoice_value: number;
  observations: string;
  start_date: string;
  effective_date: string;
  expiration_date: string;
  interest: number;
  status: string;
  identification_type: string;

}
