export interface ICreditLineAdc {
  id: number;
  approved_quota: number;
  available_quota: number;
  approval_date: string;
  expiration_date: string;
}
