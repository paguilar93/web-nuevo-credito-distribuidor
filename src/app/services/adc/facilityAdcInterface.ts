export interface IFacilityAdc {
  id: number;
  name: string;
  approved_long_term: number;
  available_long_term: number;
  blocked_short_term: number;
  used_short_term: number;
  approved_short_term: number;
  available_short_term: number;
  expiration_date: string;
}
