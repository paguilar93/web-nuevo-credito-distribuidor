import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {ICompanyAdc} from 'src/app/services/adc/companyAdcInterface';
import {ICreditLineAdc} from '../../services/adc/creditLineInterface';
import {IFacilityAdc} from '../../services/adc/facilityAdcInterface';
import {Router} from "@angular/router";
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';

@Component({
  selector: 'app-company-quota',
  templateUrl: './company-quota.component.html'
})
export class CompanyQuotaComponent implements OnInit {
  @Input() company: ICompanyAdc | null = null;

  displayedCreditLinesColumns: string[] = ['id', 'approved_quota', 'available_quota', 'approval_date', 'expiration_date'];
  firstHeadersRow: string[] = ['facilities'];
  secondHeadersRow: string[] = ['name', 'long-term', 'short-term'];
  thirdHeadersRow: string[] = ['name', 'approved_short_term', 'available_short_term', 'blocked_short_term', 'used_short_term', 'expiration_date'];
  displayedFacilitiesColumns: string[] = ['name', 'approved_short_term', 'available_short_term', 'blocked_short_term', 'used_short_term',
     'expiration_date'];
  statuses: string[] = ['Activo', 'Inactivo', 'Bloqueado'];
  creditLines: ICreditLineAdc[] = [];
  facilities: IFacilityAdc[] = [];
  @Output() Clean = new EventEmitter<boolean>();

  constructor(private router: Router) {

  }

  ngOnInit(): void {
    if (this.company) {
      console.log('company', this.company);
      if (this.company?.credit_lines.length > 0) {
        this.creditLines = this.company?.credit_lines;
        console.log('creditLines', this.creditLines);
      }
      if (this.company?.facilities.length > 0) {
        this.facilities = this.company?.facilities;
        console.log('facilities', this.facilities);
      }
    }
    registerLocaleData( es );
  }

  clean(): void {
    this.Clean.emit(true);
  }

  back(): void {
    this.router.navigateByUrl(' ').then(r => console.log('r', r));
  }

}
