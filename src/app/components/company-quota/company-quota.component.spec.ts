import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyQuotaComponent } from './company-quota.component';

describe('CompanyQuotaComponent', () => {
  let component: CompanyQuotaComponent;
  let fixture: ComponentFixture<CompanyQuotaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyQuotaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyQuotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
