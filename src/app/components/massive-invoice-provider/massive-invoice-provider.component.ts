import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {IInvoice} from '../../services/invoice/invoiceInterface';
import {Router} from "@angular/router";
import {NotifierComponent} from "../notifier/notifier.component";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {DialogComponent} from "../dialogs/dialog.component";
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';

@Component({
  selector: 'app-massive-invoice-provider',
  templateUrl: './massive-invoice-provider.component.html'
})
export class MassiveInvoiceProviderComponent implements OnInit {


  invoice: IInvoice =
    {
      id: 1,
      type: 'Factura',
      regional: 'Regional Activa',
      start_date: new Date(),
      authorization: '100005454000000000',
      value: 5505.70,
      company: 'Empresa Activa 1',
      provider: 'Proveedor Activo',
      payment_date: new Date(),
      number: '18457958413',
      observations: '',
      request: '',
      identification: '',
      identification_type: '',
      status: ''
    };
  availableSpace = 48000.00;
  totalInvoice = 2500.00;
  uploading = false;
  invoices: IInvoice[] = [
    {
      id: 1,
      type: 'Factura',
      regional: 'Regional Activa',
      start_date: new Date(),
      authorization: '100005454000000000',
      value: 6250.00,
      company: 'Empresa Activa 1',
      provider: 'Proveedor Activo',
      payment_date: new Date(),
      number: '18457958413',
      observations: 'Creado Exitosamente',
      request: '',
      identification: '17457542001',
      identification_type: 'R-P-C',
      status: 'Exitoso'
    },
    {
      id: 1,
      type: 'Factura',
      regional: 'Regional Activa',
      start_date: new Date(),
      authorization: '100005454000000000',
      value: 5550.50,
      company: 'Empresa Activa 2',
      provider: 'Proveedor 2',
      payment_date: new Date(),
      number: '18457958413',
      observations: 'Falta Empresa',
      request: '',
      identification: '17457542001',
      identification_type: 'R-P-C',
      status: 'Fallido'
    },
    {
      id: 1,
      type: 'Factura',
      regional: 'Regional Activa',
      start_date: new Date(),
      authorization: '100005454000000000',
      value: 7250.60,
      company: 'Empresa Activa 3',
      provider: 'Proveedor 3',
      payment_date: new Date(),
      number: '18457958413',
      observations: 'Creado Exitosamente',
      request: '',
      identification: '17457542001',
      identification_type: 'R-P-C',
      status: 'Exitoso'
    },
    {
      id: 1,
      type: 'Factura',
      regional: 'Regional 4',
      start_date: new Date(),
      authorization: '100005454000000000',
      value: 2500.60,
      company: 'Empresa Activa 3',
      provider: 'Proveedor 5',
      payment_date: new Date(),
      number: '18457958413',
      observations: 'Falta valor',
      request: '',
      identification: '17457542001',
      identification_type: 'R-P-C',
      status: 'Fallido'
    },
    {
      id: 1,
      type: 'Factura',
      regional: 'Regional 3',
      start_date: new Date(),
      authorization: '100005454000000000',
      value: 6250.80,
      company: 'Empresa Activa 3',
      provider: 'Proveedor 7',
      payment_date: new Date(),
      number: '18457958413',
      observations: 'Datos Incorrectos',
      request: '',
      identification: '17457542001',
      identification_type: 'R-P-C',
      status: 'Fallido'
    },

  ];
  percentage = 0;


  displayedColumns: string[] = ['authorization', 'number', 'payment_date', 'value', 'identification', 'identification_type', 'provider', 'status', 'observations'];

  @Output() Clean = new EventEmitter<boolean>();

  constructor(private router: Router, public dialog: MatDialog, private snackBar: MatSnackBar) {
    let savedValue = sessionStorage.getItem('company');
    let savedRuc = sessionStorage.getItem('ruc');
    if (savedRuc) {
      this.invoice.identification = savedRuc;
    }
    if (savedValue) {
      this.invoice.company = savedValue;
    }
  }

  ngOnInit(): void {
    registerLocaleData( es );
  }

  clean(): void {
    this.Clean.emit(true);
  }

  open_file(): void {
    const element = document.getElementById('file_input');
    console.log('file_input', element);
    if (element) {
      element.click();
    }
  }

  upload(): void {
    this.uploading = true;
    const count = setInterval(() => {
      if (this.percentage < 100) {
        this.percentage++;
      } else {
        clearInterval(count);
        this.percentage = 0;
        this.uploading = false;
      }
    }, 150);

  }

  back(): void {
    this.router.navigateByUrl(' ').then(r => console.log('r', r));
  }
  openModal(type: string): void {
    const selected = this.invoices.length;
    switch (type) {
      case 'success':
        const successDialog = this.dialog.open(DialogComponent, {
          width: '350px',
          data: {
            total: selected,
            title: '¿Está seguro de cargar?',
            body: (selected > 1 ? 'Serán cargadas ' : 'Será cargada ') + selected + (selected > 1 ? ' facturas' : ' factura'),
            button: 'Cargar'
          }
        });

        successDialog.afterClosed().subscribe(result => {
          console.log('The dialog was closed', result);
          if (result) {
            this.snackBar.openFromComponent(NotifierComponent, {
              data: {
                message: 'Carga exitosa',
                dismiss: 'Cerrar',
                type: 'Alerta'
              },
              duration: 1300,
              panelClass: 'alert-success'
            });
          }
        });

        break;
      case 'error':

        const errorDialog = this.dialog.open(DialogComponent, {
          width: '350px',
          data: {
            total: selected,
            title: '¿Está seguro de cancelar?',
            body: (selected > 1 ? 'No se cargaran ' : 'No se cargará ') + selected + (selected > 1 ? ' facturas' : ' factura'),
            button: 'Declinar'
          }
        });

        errorDialog.afterClosed().subscribe(result => {
          console.log('The dialog was closed', result);
          if (result) {
            this.snackBar.openFromComponent(NotifierComponent, {
              data: {
                message: 'Información no guardada',
                dismiss: 'Cerrar',
                type: 'Alerta'
              },
              duration: 1300,
              panelClass: 'alert-danger'
            });
          }
        });

        break;
      default:
        console.log('default');
        break;

    }
  }


}
