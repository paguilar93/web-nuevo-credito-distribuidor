import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {QuotaConsultationComponent} from './pages/quota-consultation/quota-consultation.component';
import {LoadInvoiceProviderComponent} from './pages/load-invoice-provider/load-invoice-provider.component';
import {InvoiceApprovalComponent} from './pages/invoice-approval/invoice-approval.component';
import {InvoiceConfirmationComponent} from './pages/invoice-confirmation/invoice-confirmation.component';
import {HomeComponent} from './pages/home/home.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {PrintPaymentsComponent} from "./pages/print-payments/print-payments.component";
import {CompanyProviderDetailComponent} from "./pages/company-provider-detail/company-provider-detail.component";
import {InvoicesProviderDetailComponent} from "./pages/invoices-provider-detail/invoices-provider-detail.component";
import {InvoicePaymentProviderComponent} from "./pages/invoice-payment-provider/invoice-payment-provider.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: ' ',
    component: DashboardComponent
  },
  {
    path: 'quota-consultation',
    component: QuotaConsultationComponent
  },
  {
    path: 'load-provider-invoices',
    component: LoadInvoiceProviderComponent
  },
  {
    path: 'approve-provider-invoices',
    component: InvoiceApprovalComponent
  },
  {
    path: 'confirm-provider-invoices',
    component: InvoiceConfirmationComponent
  },
  {
    path: 'print-payments',
    component: PrintPaymentsComponent
  },
  {
    path: 'company-provider-detail',
    component: CompanyProviderDetailComponent
  },
  {
    path: 'invoices-provider-detail',
    component: InvoicesProviderDetailComponent
  },
  {
    path: 'pay-provider-invoices',
    component: InvoicePaymentProviderComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
